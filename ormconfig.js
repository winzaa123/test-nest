
module.exports= {
  type: "mysql",
  database: process.env["MYSQL_DATABASE"],
  username: process.env["MYSQL_USER_APP"], // fill this with your username
  password: process.env["MYSQL_PASSWORD"], // and password
  host: process.env["MYSQL_HOST"],
  port: parseInt(process.env["MYSQL_PORT"]),
  entities: ["src/**/**.entity{.ts,.js}"],
  synchronize: process.env["NODE_ENV"] !== 'production'?true: false
}