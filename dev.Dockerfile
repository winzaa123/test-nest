# install node modules
FROM node:10.15.3 
WORKDIR /mnt/app
COPY ./nestjs-realworld-example-app /mnt/app

RUN yarn install


CMD [ "yarn", "start:watch"  ]