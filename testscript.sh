# !/bin/bash

# start the server and send the console and error logs on nodeserver.log
echo "" > nodeserver.log
yarn --cwd "$FOLDER_APP/"  start > nodeserver.log 2>&1 &

while ! grep  "Nest application successfully started" nodeserver.log
do
  cat nodeserver.log
  sleep 1
done
echo -e "server has started\n"
exit 0